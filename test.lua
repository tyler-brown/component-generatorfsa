function oldImpl()
    local Live = false
    local Auth = { ["username"] = "admin", ["password"] = "password" }
    local SourceTable = {};
    local DestinationGuid

    local _R0, _C0, H0 = net.http.post { url = "http://localhost:7654/session/login", parameters = Auth, live = Live }
    local Cookie = H0["Set-Cookie"]

    local R1,_C1,_H1 = net.http.post{ url = "http://localhost:7654/server_guid", headers = { ["cookie"] = Cookie }, live = Live }
    local ServerGuid = json.parse(R1)

    for i = 1, 2, 1
    do
        local NeuronName = { ["label"] = "#test" .. i, ["script"] = "function main() end",
                             ["server_guid"] = ServerGuid, ["type"] = "From Translator",
                             ["name"] = "test" .. i .. os.ts.time(), ["cleaned_channel_name"] = "test" .. i,
                             ["created_by"] = "admin"
        }
        local R2, _C2, _H2 = net.http.post { url = "http://localhost:7654/translator_config/add_translator",
                                           parameters = NeuronName, auth = Auth, headers = { ["cookie"] = Cookie }, live = Live }
        R2 = json.parse(R2)
        if (R2["success"] == true) then
            if (i == 1) then DestinationGuid = R2["guid"]
            else table.insert(SourceTable, R2["guid"])
            end
        end
    end

    local LinkData = { ["sender_guids"] = SourceTable, ["translator_guid"] = DestinationGuid }

    net.http.post { url = "http://localhost:7654/add_neuron_receive", body = json.serialize { data = LinkData },
                    headers = { ["cookie"] = Cookie }, live = Live }
    net.http.post { url = "http://localhost:7654/translator/restart", parameters = { ["translator_guid"] = DestinationGuid },
                    headers = { ["cookie"] = Cookie }, live = Live }

end